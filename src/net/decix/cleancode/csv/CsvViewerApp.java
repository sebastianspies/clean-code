package net.decix.cleancode.csv;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CsvViewerApp {

	private final static int DEFAULT_ENTRIES_PER_PAGE = 5;

	private static class Config {

		private Path filename;
		private int entriesPerPage;

		private Config(Path filename, int entriesPerPage) {
			this.filename = filename;
			this.entriesPerPage = entriesPerPage;

		}

		public Path getPath() {
			return filename;
		}

		public int getEntriesPerPage() {
			return entriesPerPage;
		}

		private static Config fromCmdLineArgs(String[] args) {
			if (args.length < 1) {
				throw new IllegalArgumentException("No filename given");
			}

			Path filePath = Paths.get(args[0]);

			int entriesPerPage = DEFAULT_ENTRIES_PER_PAGE;
			if (args.length > 1) {
				try {
					entriesPerPage = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					System.err.println("Could not parse page size " + e.getMessage() + " using default of "
							+ DEFAULT_ENTRIES_PER_PAGE);
				}
			}

			return new Config(filePath, entriesPerPage);

		}

	}

	public static void main(String[] args) {
		Config config = null;

		try {
			config = Config.fromCmdLineArgs(args);
		} catch (IllegalArgumentException e) {
			System.err.println("Exiting: " + e.getMessage());
			System.exit(-1);
		}

		try {
			Table csvTable = Table.fromCsvPath(config.getPath());

			ReplUi ui = new ReplUi(TableFormatter.standardTable(), config.getEntriesPerPage());

			Interactor interactor = new Interactor(ui, csvTable);

			interactor.openFirstPage();

			interactor.mainLoop();

		} catch (IOException e) {
			System.err.println("During main: " + e.getMessage());
			System.exit(-1);
		}

	}

}
