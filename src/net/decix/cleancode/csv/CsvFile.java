package net.decix.cleancode.csv;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CsvFile {

	private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
	private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
	private static final String NUMBER = "0123456789";
	private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;

	private static final int INPUT_SIZE = 25000000;
	private static final int SPLIT_SIZE = 10000000;

	private Path csvPath;
	private AtomicBoolean isDoneIndexing = new AtomicBoolean(false);
	private static Random random = new Random();

	private AtomicInteger recordsCount = new AtomicInteger(0);

	public CsvFile(Path csvPath) {
		this.csvPath = csvPath;

		try {
			initializeSortIndices();
		} catch (Exception e) {
			System.err.println("Warning, could not create search indices: " + e.getMessage());
		}

	}

	public static void generateFile(Path csvPath) throws IOException {

		try (BufferedWriter writer = Files.newBufferedWriter(csvPath, Charset.forName("UTF-8"))) {
			for (int i = 0; i < INPUT_SIZE; i++) {
				String generatedString = generateRandomString(8);
				writer.append(String.valueOf(i));
				writer.append(';');
				writer.append(generatedString);
				writer.append(';');
				writer.append(String.valueOf(generateRandomInt(50)));
				writer.newLine();
			}
		}
	}

	private Path generateIndexPath(String column) {
		return Paths.get(String.format(".%s.index.%s", csvPath.getFileName(), column));
	}

	private String generateIndexSearchPath(String column) {
		return String.format(".%s.%s.*", csvPath.getFileName(), column);
	}

	private Path generateLastAccessPath() {
		return Paths.get(String.format(".%s.lastmodified", csvPath.getFileName()));
	}

	private Path generateLineOffsetPath() {
		return Paths.get(String.format(".%s.lineoffsets", csvPath.getFileName()));
	}

	private static int generateRandomInt(int maxSize) {
		return random.nextInt(maxSize);
	}

	private static String generateRandomString(int length) {
		if (length < 1)
			throw new IllegalArgumentException();

		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++) {

			// 0-62 (exclusive), random returns 0-61
			int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
			char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

			// debug
//			System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

			sb.append(rndChar);

		}

		return sb.toString();

	}

	private Path generateSplitPath(int split, String column) {
		return Paths.get(String.format(".%s.%s.%d", csvPath.getFileName(), column, split));
	}

	public int getRecordsCount() {
		return recordsCount.get();
	}

	protected void initializeSortIndices() throws Exception {

		List<String> schema = List.of("id", "name", "age");

		if (wasModified()) {
			System.out.println("was modified");

			
			// TODO don't put this stuff into RAM - persist while read
			System.out.println("reading line offsets");
			List<Integer> lineOffsets = readOffsetsOfLine();

			System.out.println("writing offsets");
			persistLineOffsets(lineOffsets);

			System.out.println("split and sort");
			splitAndSort(schema);

			System.out.println("merge split");
			mergeSplitFiles(schema);

			System.out.println("Writing last modified");
			writeLastModified();
		}

		isDoneIndexing.set(true);

	}

	public boolean isDoneIndexing() {
		return isDoneIndexing.get();
	}

	private void mergeSplitFiles(List<String> schema) throws IOException {

		for (String column : schema) {
			Path indexPath = generateIndexPath(column);
			try (DataOutputStream os = new DataOutputStream(
					new BufferedOutputStream(Files.newOutputStream(indexPath)))) {
				try (DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get("."),
						generateIndexSearchPath(column))) {
					Map<BufferedReader, String> readers = new HashMap<>();

					for (Path path : paths) {
						readers.put(Files.newBufferedReader(path), null);
					}

					// Initialize
					Iterator<Entry<BufferedReader, String>> entrySetIterator = readers.entrySet().iterator();
					while (entrySetIterator.hasNext()) {
						Entry<BufferedReader, String> entry = entrySetIterator.next();
						BufferedReader reader = entry.getKey();
						String nextValue = reader.readLine();
						if (nextValue != null) {
							entry.setValue(nextValue);
						} else {
							reader.close();
							entrySetIterator.remove();
						}
					}

					do {
						// find smallest entry
						Entry<BufferedReader, String> smallestEntry = Collections.min(readers.entrySet(),
								(one, another) -> one.getValue().compareTo(another.getValue()));

						// Write smallest entry (record index only) to file
						os.writeInt(Integer.parseInt(smallestEntry.getValue().split(";")[1]));

						// read next entry or close and remove reader
						BufferedReader fileOfSmallestEntry = smallestEntry.getKey();
						String nextValue = fileOfSmallestEntry.readLine();
						if (nextValue != null) {
							smallestEntry.setValue(nextValue);
						} else {
							fileOfSmallestEntry.close();
							readers.remove(fileOfSmallestEntry);
						}
					} while (readers.size() > 0);

				}
			}

			try (DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get("."),
					generateIndexSearchPath(column))) {
				paths.forEach(f -> {
					try {
						Files.deleteIfExists(f);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
			}
		}

	}

	private void persistLineOffsets(List<Integer> lineOffsets) throws IOException {
		Path lineOffsetPath = generateLineOffsetPath();

		try (DataOutputStream os = new DataOutputStream(
				new BufferedOutputStream(Files.newOutputStream(lineOffsetPath)))) {
			for (int offset : lineOffsets) {
				os.writeInt(offset);
			}
		}
	}

	private List<Integer> readIndices(String column, int offset, int numberOfEntries) throws IOException {
		Path indexPath = generateIndexPath(column);
		List<Integer> indices = new ArrayList<>();
		try (DataInputStream ois = new DataInputStream(Files.newInputStream(indexPath))) {
			ois.skipBytes(offset * Integer.BYTES);
			for (int i = 0; i < numberOfEntries; i++) {
				indices.add(ois.readInt());
			}
		}
		return indices;
	}
	
	public List<String> readLinesOrdered(String column, int offset, int numberOfEntries) throws IOException {
		List<Integer> indices = readIndices(column, offset, numberOfEntries);
		return readLines(indices);
	}

	public List<String> readLines(List<Integer> lineNumbers) throws IOException {

		try (FileChannel lineOffsetFileChannel = FileChannel.open(generateLineOffsetPath())) {
			Map<Integer, Integer> sequentialOffsets = lineNumbers.stream().map(lineNumber -> {
				try {
					return Map.entry(readOffset(lineOffsetFileChannel, lineNumber), lineNumber);
				} catch (IOException e) {
					throw new UncheckedIOException(e);
				}
			}).collect(Collectors.toMap(Entry::getKey, Entry::getValue, (v1, v2) -> v1, TreeMap::new));

			Map<Integer, String> lineNumberToLine = new HashMap<>();

			try (FileChannel csvFileChannel = FileChannel.open(csvPath)) {

				ByteBuffer buffer = ByteBuffer.allocate(50000000);

				for (Entry<Integer, Integer> offsetToLineNumber : sequentialOffsets.entrySet()) {
					Integer offsetOfLine = offsetToLineNumber.getKey();
					int lineNumber = offsetToLineNumber.getValue();
					Integer offsetOfNextline = readOffset(lineOffsetFileChannel, lineNumber + 1);
					int lineLength = offsetOfNextline - offsetOfLine - 1;
					buffer.rewind();
					csvFileChannel.read(buffer, offsetOfLine);

					// TODO check if buffer has read enough bytes
					String line = new String(buffer.array(), 0, lineLength);
					lineNumberToLine.put(lineNumber, line);

				}

			}
			return lineNumbers.stream().map(lineNumberToLine::get).collect(Collectors.toList());
		}

	}

	private int readOffset(FileChannel channel, int line) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
		channel.position(Integer.BYTES * line).read(buffer);
		return buffer.rewind().asIntBuffer().get(0);
	}

	private List<Integer> readOffsetsOfLine() throws IOException {
		try (FileChannel c = FileChannel.open(csvPath)) {
			ByteBuffer buffer = ByteBuffer.allocate(100000000);
			List<Integer> lineOffsets = new ArrayList<>();

			int offset = 0;
			int bytesRead = 0;

			lineOffsets.add(offset);

			while ((bytesRead = c.read(buffer)) > 0) {
				int subOffset = 0;
				buffer.rewind();

				for (int i = 0; i < bytesRead; i++) {
					byte b = buffer.get();

					if ((char) b == '\n') {
						lineOffsets.add(offset + subOffset + 1);
					}
					++subOffset;
				}
				buffer.rewind();
				offset += bytesRead;
			}

			return lineOffsets;
		}
	}

	private void sortAndWriteToSplitFiles(int split, Map<String, List<String>> sortedLinesMaps) throws IOException {
		for (Map.Entry<String, List<String>> sortedEntry : sortedLinesMaps.entrySet()) {
			List<String> sortedLines = sortedEntry.getValue();
			Collections.sort(sortedLines);
			Path splitPath = generateSplitPath(split, sortedEntry.getKey());
			writeToSplitFile(splitPath, sortedLines);
		}
	}

	private static void writeToSplitFile(Path splitPath, List<String> sortedLines) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(splitPath)) {
			for (String sortedLine : sortedLines) {
				writer.write(sortedLine);
				writer.newLine();
			}
		}
	}

	private void splitAndSort(List<String> schema) throws IOException {

		try (BufferedReader reader = Files.newBufferedReader(csvPath)) {
			int lines = 0, split = 0;

			Map<String, List<String>> sortedLines = schema.stream()
					.map(name -> Map.entry(name, new LinkedList<String>()))
					.collect(Collectors.toMap(Entry::getKey, Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));

			String line;
			while ((line = reader.readLine()) != null) {

				if (++lines < SPLIT_SIZE) {
					String[] columns = line.split(";");
					int i = 0;

					for (List<String> sortedColumn : sortedLines.values())
						sortedColumn.add(columns[i++] + ";" + columns[0]);
				} else {
					sortAndWriteToSplitFiles(split++, sortedLines);
					lines = 0;
					for (List<String> sortedColumn : sortedLines.values())
						sortedColumn.clear();
				}
			}

			if (!sortedLines.isEmpty())
				sortAndWriteToSplitFiles(split++, sortedLines);
		}
	}

	public boolean wasModified() {
		Path csvLastAccessedPath = generateLastAccessPath();

		if (!Files.exists(csvPath)) {
			return true;
		}

		try (ObjectInputStream oos = new ObjectInputStream(Files.newInputStream(csvLastAccessedPath))) {
			Instant lastModifiedTime = (Instant) oos.readObject();
			if (!lastModifiedTime.equals(Files.getLastModifiedTime(csvPath).toInstant())) {
				return true;
			}
		} catch (IOException | ClassNotFoundException e) {
			return true;
		}

		return false;
	}

	private void writeLastModified() throws IOException {
		Path csvLastAccessedPath = generateLastAccessPath();

		Instant lastModifiedTime = Files.getLastModifiedTime(csvPath).toInstant();

		try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(csvLastAccessedPath))) {
			oos.writeObject(lastModifiedTime);
		}
	}

	public static void main(String[] args) throws IOException {
//		CsvFile.generateFile(Paths.get("something.csv"));
		CsvFile csvFile = new CsvFile(Paths.get("something.csv"));
		
		for (String line : csvFile.readLines(List.of(999999, 0, 1, 2, 300, 3823948))) {
			System.out.println(line);
		}
		
		for (String line : csvFile.readLinesOrdered("name", 999999, 20)) {
			System.out.println(line);
		}

	}
}
