package net.decix.cleancode.csv;

import java.io.IOException;

public class Interactor {

	private ReplUi ui;
	private Table table;

	private int currentPage = 0;

	// turn around the dependency ui <-> Interactor
	public Interactor(ReplUi ui, Table table) {
		this.ui = ui;
		this.table = table;
	}

	public void mainLoop() {

		try {
			printCurrentPage();

			// better: use an enum for representing the user intent
			String input;
			do {
				input = ui.readInput();

				// better to pass the current page object through to printCurrentPage()
				switch (input.toUpperCase()) {
				case "N":
					openNextPage();
					break;

				case "P":
					openPreviousPage();
					break;

				case "F":
					openFirstPage();
					break;

				case "L":
					openLastPage();
					break;

				case "J":
					jumpToPage();
					break;

				case "S":
					sortByColumn();
					break;
				}
				
				printCurrentPage();

			} while (!input.equalsIgnoreCase("X"));
			
			System.out.println("Exited main loop.");
		} catch (IOException e) {
			System.err.println("Cannot read input from user interface: " + e.getMessage());
			System.exit(-1);
		}
	}

	public void openNextPage() {
		currentPage = wrapPage(++currentPage);

	}

	public void openPreviousPage() {
		currentPage = wrapPage(--currentPage);
	}

	public void openFirstPage() {
		currentPage = 0;
	}

	public void openLastPage() {
		currentPage = ui.calculateLastPage(table);
	}

	// Kata II.3 - Let the user jump to a page by entering its number.
	public void jumpToPage() {
		ui.printJumpToPagePrompt();
		try {
			currentPage = wrapPage(ui.readNumberInput() - 1);
		} catch (NumberFormatException | IOException e) {
			System.err.println("Not a valid page: " + e.getMessage());
		}

	}

	// Kata IIa - Sort by column
	public void sortByColumn() {
		ui.printSortByColumnPrompt();

		String sortByColumn;
		try {
			sortByColumn = ui.readInput();
			table.sortBy(sortByColumn);
		} catch (Exception e) {
			System.err.println("No valid input: " + e.getMessage());
		}
		
	}

	public void printCurrentPage() {
		ui.printPage(currentPage, table, ui.calculateLastPage(table));
	}

	private int wrapPage(int page) {
		if (page < 0) {
			return ui.calculateLastPage(table);
		}
		return page % (ui.calculateLastPage(table) + 1);
	}

}
