package net.decix.cleancode.csv;

import java.io.Console;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ReplUi {

	private TableFormatter formatter;
	private int entriesPerPage;
	private Console console;

	public ReplUi(TableFormatter formatter, int entriesPerPage) {
		this.formatter = formatter;
		this.entriesPerPage = entriesPerPage;
		this.console = System.console();
	}

	public void printPage(int page, Table table, int maxPage) {
		List<String> columnNames = table.getSchema();
		List<Map<String, String>> pageData = table.getData().subList(calculateOffset(page),
				calculateEndOfPage(page, table));

		String output = formatter.formatPage(columnNames, pageData, page, maxPage);
		System.out.println(output);
	}

	public void printJumpToPagePrompt() {
		System.out.println(formatter.formatJumpToPagePrompt());
	}

	public void printSortByColumnPrompt() {
		System.out.println(formatter.formatSortByColumnPrompt());
	}

	public String readInput() throws IOException {
		return console.readLine();
//		return Character.toString(System.in.read());
	}

	public int readNumberInput() throws IOException {
		return Integer.parseInt(readInput());
	}

	public int calculateEndOfPage(int page, Table table) {
		return Math.min(calculateOffset(page) + entriesPerPage, table.data.size());
	}

	private int calculateOffset(int page) {
		return page * entriesPerPage;
	}

	public int calculateLastPage(Table table) {
		return table.getData().size() / entriesPerPage;
	}
}
