package net.decix.cleancode.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Table {

	private static final String RECORD_INDEX_KEY = "No.";
	protected List<String> schema;
	protected List<Map<String, String>> data;

	protected Table(List<String> schema, List<Map<String, String>> data) {
		this.schema = schema;
		this.data = data;
	}

	protected static List<String> parseSchema(String line) {
		return tokenize(line);
	}

	public List<Map<String, String>> getData() {
		return data;
	}

	protected void setData(List<Map<String, String>> data) {
		this.data = data;
	}

	public List<String> getSchema() {
		return schema;
	}

	protected void setSchema(List<String> schema) {
		this.schema = schema;
	}

	public static Table fromCsvPath(Path path) throws IOException {

		String schemaLine = extractSchemaLine(path);
		Stream<String> dataLines = extractDataLines(path);

		List<String> schema = readSchemaFromLine(schemaLine);
		List<Map<String, String>> data = readDataFromLines(schema, dataLines);

		// Kata II.1 - Each data record should be preceded by a record number.
		addRecordIndexToSchema(schema);
		addRecordIndexToData(data);
		// end of Kata II.1

		return new Table(schema, data);
	}

	private static void addRecordIndexToData(List<Map<String, String>> data) {
		final AtomicInteger index = new AtomicInteger(1);
		data.stream().forEach(row -> row.put(RECORD_INDEX_KEY, Integer.toString(index.getAndIncrement())));
	}

	private static void addRecordIndexToSchema(List<String> schema) {
		schema.add(0, RECORD_INDEX_KEY);

	}

	private static Stream<String> extractDataLines(Path path) throws IOException {
		return Files.lines(path).skip(1);
	}

	private static String extractSchemaLine(Path path) throws IOException {
		return Files.lines(path).limit(1).findFirst().orElseThrow();
	}

	/*
	 * Operation
	 */
	protected static List<String> readSchemaFromLine(String line) {
		return tokenize(line);
	}

	protected static List<Map<String, String>> readDataFromLines(List<String> schema, Stream<String> lines)
			throws IOException {

		final List<Map<String, String>> data = new ArrayList<>();

		lines.forEach(line -> {
			List<String> tokens = tokenize(line);

			Map<String, String> row = new HashMap<>();
			for (int i = 0; i < tokens.size(); i++) {
				row.put(schema.get(i), tokens.get(i));
			}
			data.add(row);
		});

		return data;
	}

	protected static List<String> tokenize(String line) {
		return new ArrayList<String>(Arrays.asList(line.split(";", -1)));
	}

	public void sortBy(String sortByColumn) {
		if (!schema.contains(sortByColumn)) {
			throw new IllegalArgumentException(sortByColumn + " does not exist");
		}

		data.sort((row1, row2) -> row1.get(sortByColumn).compareTo(row2.get(sortByColumn)));
	}

}
