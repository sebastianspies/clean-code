package net.decix.cleancode.locstat;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class LocCounter {

	private static final Path sumPath = Paths.get("sum");
	private static final String CODE_FILENAME_SUFFIX = ".java";
	private static final Pattern COMMENT_PATTERN = Pattern.compile("^\\s*(//|#).*$", Pattern.DOTALL);

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.err.println("Please provide a directory name");
			System.exit(-1);
		}

		Path directory = Paths.get(args[0]);

		long before = System.currentTimeMillis();
		LocStat total = computeTotal(directory);
		System.out.println("sequential: " + (System.currentTimeMillis() - before));

		
		before = System.currentTimeMillis();
		computeTotalParallel(directory);
		System.out.println("parallel: " + (System.currentTimeMillis() - before));
		
		System.out.println(total);

	}
	
	private static LocStat computeTotal(Path directory) throws IOException {
		return Files.walk(directory)
				.filter(Files::isRegularFile)
				.filter(Files::isReadable)
				.filter(LocCounter::isJavaFile)
				.map(LocCounter::readFile)
				.map(LocCounter::analyze)
				.peek(LocCounter::printOnly)
				.reduce(new LocStat(sumPath, 0, 0), (subtotal, element) -> new LocStat(sumPath,
						subtotal.getLoc() + element.getLoc(), subtotal.getTotal() + element.getTotal()));
	}

	private static LocStat computeTotalParallel(Path directory) throws IOException {
		return Files.walk(directory).parallel()
				.filter(Files::isRegularFile)
				.filter(Files::isReadable)
				.filter(LocCounter::isJavaFile)
				.map(LocCounter::readFile)
				.map(LocCounter::analyze)
				.peek(LocCounter::printOnly)
				.reduce(new LocStat(sumPath, 0, 0), (subtotal, element) -> new LocStat(sumPath,
						subtotal.getLoc() + element.getLoc(), subtotal.getTotal() + element.getTotal()));
	}

	private static boolean isJavaFile(Path path) {
		return path.getFileName().toString().endsWith(CODE_FILENAME_SUFFIX);
	}

	private static void printOnly(LocStat stat) {
		System.out.println(stat);
	}

	private static LocStat analyze(Entry<Path, List<String>> entry) {
		int loc = 0;
		int total = 0;

		for (String line : entry.getValue()) {
			++total;

			if (!COMMENT_PATTERN.matcher(line).matches()) {
				++loc;
			}

		}
		return new LocStat(entry.getKey(), loc, total);
	}

	private static Entry<Path, List<String>> readFile(Path path) {
		try {
			return Map.entry(path, Files.readAllLines(path));
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}

	}
}
