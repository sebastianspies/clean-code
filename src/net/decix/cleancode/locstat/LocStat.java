package net.decix.cleancode.locstat;

import java.nio.file.Path;

public class LocStat {

	private Path filename;
	private int loc;
	private int total;

	LocStat(Path filename, int loc, int total) {
		this.filename = filename;
		this.loc = loc;
		this.total = total;

	}

	public Path getFilename() {
		return filename;
	}

	public int getLoc() {
		return loc;
	}

	public int getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s", filename, loc, total);
	}
}
