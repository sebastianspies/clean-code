package net.decix.cleancode.csv;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.decix.cleancode.csv.Interactor;
import net.decix.cleancode.csv.ReplUi;
import net.decix.cleancode.csv.Table;
import net.decix.cleancode.csv.TableFormatter;

public class InteractorTest {

	private static class ReplUiMock extends ReplUi {
		private boolean printPageCalled = false;

		public boolean isPrintPageCalled() {
			return printPageCalled;
		}

		public ReplUiMock(TableFormatter formatter, int entriesPerPage) {
			super(formatter, entriesPerPage);
		}

		@Override
		public void printPage(int page, Table table, int maxPage) {
			super.printPage(page, table, maxPage);
		}
	}

	@Test
	public void testPrintCurrentPage() {
		
		ReplUiMock ui = new ReplUiMock(null, 5);
		Interactor interactor = new Interactor(ui, null);
		

		
		interactor.printCurrentPage();
		
		assertTrue(ui.isPrintPageCalled());
		

	}

}
